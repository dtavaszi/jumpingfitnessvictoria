module StaticPagesHelper

  def gallery_images_array
    [
      { name: 'birthday.jpg', description: 'Birthday party' },
      { name: 'foam.jpg', description: '5K Foam Fest 2018' },
      { name: 'group.jpg', description: 'Princess run' },
      { name: 'group_blurry.jpg', description: 'Jumping Fitness team' },
      { name: 'party.jpg', description: 'Jumping Fitness Party' },
      { name: 'workout.jpg', description: 'Workout'}
    ]
  end
end
